#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

using namespace std;

char number[10] = {'1','2','3','4','5','6','7','8','9'};
bool result = false;
char symbole = 'X';
QString stringSymbol;
int player = 1;
int choice ;
int NumberOfmove = 0;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
        ui->setupUi(this);
        connect(ui->pushButton_1,SIGNAL(released()), this,SLOT(makeChoice()));
        connect(ui->pushButton_2,SIGNAL(released()), this,SLOT(makeChoice()));
        connect(ui->pushButton_3,SIGNAL(released()), this,SLOT(makeChoice()));
        connect(ui->pushButton_4,SIGNAL(released()), this,SLOT(makeChoice()));
        connect(ui->pushButton_5,SIGNAL(released()), this,SLOT(makeChoice()));
        connect(ui->pushButton_6,SIGNAL(released()), this,SLOT(makeChoice()));
        connect(ui->pushButton_7,SIGNAL(released()), this,SLOT(makeChoice()));
        connect(ui->pushButton_8,SIGNAL(released()), this,SLOT(makeChoice()));
        connect(ui->pushButton_9,SIGNAL(released()), this,SLOT(makeChoice()));
}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::makeChoice()
{
        stringSymbol = "";
        stringSymbol += symbole;
        QPushButton * button = (QPushButton*)sender();

        if(button->objectName() == "pushButton_1" && button->text() == "" ) {
            number[0] = symbole;
            ui->pushButton_1->setText(stringSymbol);
            weHaveAWinner();
        }else if(button->objectName() == "pushButton_2" && button->text() == "" ){
            number[1] = symbole;
            ui->pushButton_2->setText(stringSymbol);
            weHaveAWinner();
        }else if(button->objectName() == "pushButton_3" && button->text() == "" ){
            number[2] = symbole;
            ui->pushButton_3->setText(stringSymbol);
            weHaveAWinner();
        }else if(button->objectName() == "pushButton_4" && button->text() == "" ){
            number[3] = symbole;
            ui->pushButton_4->setText(stringSymbol);
            weHaveAWinner();
        }else if(button->objectName() == "pushButton_5" && button->text() == "" ){
            number[4] = symbole;
            ui->pushButton_5->setText(stringSymbol);
            weHaveAWinner();
        }else if(button->objectName() == "pushButton_6" && button->text() == "" ){
            ui->pushButton_6->setText(stringSymbol);
            number[5] = symbole;

            weHaveAWinner();
        }else if(button->objectName() == "pushButton_7" && button->text() == "" ){
            number[6] = symbole;
            ui->pushButton_7->setText(stringSymbol);
            weHaveAWinner();
        }else if(button->objectName() == "pushButton_8" && button->text() == "" ){
            number[7] = symbole;
            ui->pushButton_8->setText(stringSymbol);
            weHaveAWinner();
        }else if(button->objectName() == "pushButton_9" && button->text() == "" ){
            number[8] = symbole;
            ui->pushButton_9->setText(stringSymbol);
            weHaveAWinner();
        }
}


void MainWindow::weHaveAWinner(){
    QPushButton * button = (QPushButton*)sender();
    if(button->text() == "X" || button->text() == "O"){
        NumberOfmove ++;

        if(number[0] == number[1] && number[1] == number[2]){
            result = true;
        }else if(number[3] == number[4] && number[4] == number[5]){
            result = true;
        }else if(number[6] == number[7] && number[7] == number[8]){
            result = true;
        }else if(number[0] == number[3] && number[3] == number[6]){
            result = true;
        }else if(number[1] == number[4] && number[4] == number[7]){
            result = true;
        }else if(number[2] == number[5] && number[5] == number[8]){
            result = true;
        }else if(number[0] == number[4] && number[4] == number[8]){
            result = true;
        }else if(number[2] == number[4] && number[4] == number[6]){
            result = true;
        }else if(NumberOfmove == 9){
             ui->label1->setText("Egaliter");
             BlockButton();
        }
       if (result == false) {

       }
       else
       {
           if(player == 1){
               ui->label1->setText("Player 1 WIN");
           }else{
               ui->label1->setText("Player 2 WIN");
           }
            BlockButton();
       }

       switch (player)
        {
        case 1:
            symbole = 'O';
            player = 2;
            break;

        case 2:
            symbole =  'X';
            player = 1;
            break;
        }
    }
}

void MainWindow::BlockButton(){
    ui->pushButton_1->setEnabled(false);
    ui->pushButton_2->setEnabled(false);
    ui->pushButton_3->setEnabled(false);
    ui->pushButton_4->setEnabled(false);
    ui->pushButton_5->setEnabled(false);
    ui->pushButton_6->setEnabled(false);
    ui->pushButton_7->setEnabled(false);
    ui->pushButton_8->setEnabled(false);
    ui->pushButton_9->setEnabled(false);
}
